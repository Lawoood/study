import random
def getCoef(l, n) :
    s = 0
    for i in range((n**2) +1) :
        s += i
    return s

def isRightDiag(l, n, s) :
    ll = list()
    if n == 1 :
        for i in range(len(l[0])) :
                ll.append(l[i][i])
    else :
        for i in range(0, len(l)):
            j = len(l[0]) - i - 1
            ll.append(l[i][j])
    sol = 0
    for i in range(len(ll)) :
        sol += ll[i]
    return sol == s

def isRightColum(l, n, s) :
    ll = list()
    for i in range(len(l[0])) :
        ll.append(l[n][i])
    sol = 0
    for i in range(len(ll)) :
        sol += ll[i]
    return sol == s

def isRightRow(l, n, s) :
    ll = l[n]
    sol = 0
    for i in range(len(ll)) :
        sol += ll[i]
    return sol == s

def magicSquare(l,n) :
    s = getCoef(l, n)
    s //= n
    b = True
    for i in range(0,3) :
        b = b and isRightColum(l, i, s)
        b = b and isRightRow(l, i, s)
    for i in range(0,2) :
        b = b and isRightDiag(l, i, s)
    return b
l = [[6,7,2],[1,5,9],[8,3,4]]
print(magicSquare(l, 3))