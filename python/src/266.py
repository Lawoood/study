def org(l) :
    s = [0,0,0]
    for n in l :
        if n == 1 :
            s[0] += 1
        elif n == 2 :
            s[1] += 1
        else :
            s[2] += 1
    for n in l :
        if n < s[0] :
            l[n] = 1
        elif n < s[2] :
            l[n] = 2
        else :
            l[n] = 3
    return




def sortlist(l) :
    for n in range(0,len(l) - 1) :
        if l[n] > l[n+1] :
            l[n], l[n+1] = l[n+1], l[n]
            sortlist(l)
    return
ll = [3,6,9,4]
org(ll)
print(ll)