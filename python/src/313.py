def getSomOfCube(n) :
    if n > 0 :
        return n**2 + getSomOfCube(n - 1)
    else :
        return n**2
print(getSomOfCube(3))