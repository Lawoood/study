def getIntUserImput(a, b, s) :
    az = a - 1;
    while (az < a or az > b) :
        az = int(input(s))
    return az
def dispMap(l) :
    for i in range(len(l)) :
        for j in range(len(l[0])) :
            if l[i][j] == 0 :
                print(" . ",end="")
            elif l[i][j] == 1 :
                print(" x ",end="")
            elif l[i][j] == 2 :
                print(" O ",end="")
        print()
    return
def changePlayer(n) :
    if (n == 1) :
        return 2
    else :
        return 1
def checkLine(l) :
    for i in range(0,3) :
        if l[i][0] == l[i][1] == l[i][2] and l[i][0] != 0:
            return True
    return False

def checkColum(l) :
    for i in range(0,3) :
        if l[0][i] == l[1][i] == l[2][i] and l[0][i] != 0:
            return True
    return False
def checkDiag(l) :
    if l[0][0] == l[1][1] == l[2][2] and l[0][0] != 0:
            return True
    if l[0][2] == l[1][1] == l[2][0] and l[1][1] != 0:
            return True
    return False
def getWinner(round, l) :
    b = True
    for i in range(len(l)) :
        b = b and checkLine(l) or checkColum(l) or checkDiag(l)
    if b :
        print("Player", changePlayer(round), "won")
        return 1
    else :
        return 0
    return 0
def play(l, round) :
    b = False
    while not b :
        pos = (getIntUserImput(1, 3, "Wich line?"), getIntUserImput(1, 3, "Wich colum?"))
        if l[pos[0] - 1][pos[1] - 1] == 0 :
            b = True
            l[pos[0] - 1][pos[1] - 1] = round
    return
def morpion() :
    l = [[0] * 3 for i in range(0,3)]
    round = getIntUserImput(1,2, "Get who start?")
    dispMap(l)
    while getWinner(round, l) == 0 :
        print("Player", round, "it's your turn")
        play(l, round)
        dispMap(l)
        round = changePlayer(round)
    return
morpion()