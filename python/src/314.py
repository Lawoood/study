def findIn(l, a) :
    if len(l) == 0 :
        return False
    elif (l[0] == a) :
        return True
    else :
        findIn(l[1::], a)
    return False
print(findIn([1,2,3],2))