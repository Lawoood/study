def isDivisor(a, b):
    return a % b == 0

def sumOfDivisor(n):
    res = 0
    for j in range(n // 2, 0, -1):
        if isDivisor(n, j):
            res += j
    return res

def isPerfect(n):
    return n == sumOfDivisor(n)

def dispPerfectAt(n) :
    for j in range(0, n):
        print("Testing", j)
        if isPerfect(j):
            print(j, "is Perfect")
    return
dispPerfectAt(10000000000000000000000000000000000000000)