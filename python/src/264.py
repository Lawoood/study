def getElem(l, i) :
    for j,s in enumerate(l) :
        if s == i :
            return j
    return -1
def getMinInd(l) :
    n = l[0]
    for s in l :
        if s < n :
            n = s
    return getElem(l, n)
print(getMinInd([1,23,-1]))