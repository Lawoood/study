import random

def getNbrOfPlayer() :
    a = -1
    while a != 1 and a != 2 :
        a = int(input("How many player 1 or 2 ?"))
    return a

def getNumberOfMatchs() :
    a = 0
    while a % 2 != 1 :
        a = int(input("Take odd number?"))
    return a

def dispMap(n) :
    if (n > 1) :
        print("There is", n, "matchs")
    else :
        print("There is", n, "match")
    print("| " * n)

def getMatchAfterTaken(match, n) :
    return match - n

def getMatchToRemoveIA() :
    return random.randint(1, 3)

def getMatchToRemove(match) :
    a = 0
    while a < 1 or a > match or a > 3:
        a = int(input("How many do you want to remove ?\n"))
    return a
def getWhoStart() :
    a = -1
    while a != 1 and a != 2:
        a = int(input("Who should start YOU=1 HIM=2"))
    return a
def changePlayer(n) :
    if (n == 1) :
        return 2
    else :
        return 1
def startGame() :
    player = getNbrOfPlayer()
    match = getNumberOfMatchs()
    round = changePlayer(getWhoStart())
    ss = 0
    while match > 0:
        round = changePlayer(round)
        print("Player", round, "it's your turn")
        dispMap(match)
        if player == 1 and round == 2:
            ss = getMatchToRemoveIA()
            print("IA took ", ss,"match")
            match -= ss
        else:
            match -= getMatchToRemove(match)
    print("Player", changePlayer(round), "won")
    return
startGame()
