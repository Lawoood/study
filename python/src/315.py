def findIn(l, a) :
    if len(l) == 0 :
        return -1
    if (l[0] == a) :
        return 0
    b = findIn(l[1::], a)
    if b == 1:
        return 1 + b
    elif b == -1 :
        return -1
    return -1
print(findIn([1,2,3],3))

"""NON FONCTIONEL"""